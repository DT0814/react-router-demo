import React, { Component } from 'react';
import '../styles/App.css';
import { BrowserRouter as Router, Link, NavLink, Redirect, Route, Switch } from 'react-router-dom';
import Home from "./Home";
import MyProfile from "./MyProfile";
import AboutUs from "./AboutUs";
import Products from "./Products";
import ProductDetails from "./ProductDetails";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: false,
    };
    this.clickUrl = this.clickUrl.bind(this)
  }

  clickUrl(event) {
    console.log(123)
  }

  render() {
    return (
      <div className="app">
        <Router>
          <nav>
            <ul>
              <li>
                <NavLink exact to='/' activeClassName={'selected'} onClick={this.clickUrl}><span>Home</span></NavLink>
              </li>
              <li>
                <NavLink activeClassName={'selected'} to="/products"
                         onClick={this.clickUrl}><span>products</span></NavLink>
              </li>
              <li>
                <NavLink activeClassName={'selected'} to="/about" onClick={this.clickUrl}><span>About</span></NavLink>
              </li>
              <li>
                <NavLink activeClassName={'selected'} to="/my-porfile"
                         onClick={this.clickUrl}><span>MyProfile</span></NavLink>
              </li>
            </ul>
          </nav>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/(products|goods)"  component={Products}/>
            <Route path="/productsDetails/:id" component={ProductDetails}/>
            <Route path="/my-porfile" component={MyProfile}/>
            <Route path="/about" component={AboutUs}/>
            <Redirect path="/" to={{pathname: '/'}} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
